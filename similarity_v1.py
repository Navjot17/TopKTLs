from pymongo import MongoClient
import nltk
from nltk.corpus import stopwords
stop = set(stopwords.words('english'))

connection = MongoClient('localhost', 27017)
db = connection.pubData
collection = db.publications
publications = collection.find()

count = 0
###while true? try to print objectid
for doc in publications:
	titleWords = []
	try:
		title = doc['article_title']
		titleWords = [word.lower() for word in nltk.word_tokenize(title) if word.lower() not in stop and len(word)>=2]
		#print titleWords
	except:
		pass
	nouns = []
	try:
		abstract = doc["abstract"]
		tokens = nltk.word_tokenize(abstract.lower())
		tagged = nltk.pos_tag(tokens)
		#print tagged
		try:
	 		nouns = [str(a).lower() for (a, b) in tagged if b[:2] == 'NN' and len(a)>=2]
		except:		
			nouns = []
		#print nouns
	except:
		pass
	meshTerms = []
	try:
		meshTerms = []
		for term in doc['mesh_terms']:
			term = term.split('/')
			for term1 in term:
				meshTerms += [t.strip() for t in term1.split(',')]
		#print meshTerms
	except:
		pass
	count = count + 1
	if count%100 == 0:
		print(count)
		print(doc['_id'])
	#print doc['mesh_terms']
	#print "---------------------"
	#raw_input('next?')
	nouns = list(set(nouns))
	titleWords = list(set(titleWords))
	meshTerms = list(set(meshTerms))
	collection.update({'_id':doc['_id']},{"$set":{"abstractNouns":nouns,"titleWords":titleWords,"meshTerms_new":meshTerms}})


