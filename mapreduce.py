from bson.code import Code
from pymongo import MongoClient
conn = MongoClient('localhost', 27017)
db = conn.pubData
collection = db.publications



map = Code("function() {"
	"  this.abstractNouns.forEach(function(z) {"
	"    emit(z, 1);"
	"  });"
	"}")

	
reduce = Code("function (key, values) {"
              "  var total = 0;"
              "  for (var i = 0; i < values.length; i++) {"
              "    total += values[i];"
              "  }"
              "  return total;"
              "}")


collection.map_reduce(map, reduce, "abstractNouns_count", query={"abstractNouns":{"$exists":True}})
